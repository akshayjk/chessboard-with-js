console.clear();

let fontOne = 'White', fontTwo = 'Black'; // 2 variables represent two grids

let boardOrder = 8; // enter the order of the chessboard.. we will decrement by one if its an odd number

if (boardOrder % 2 !== 0) {
    boardOrder -= 1;
    console.log('Sorry, the order of grid should be always even.\nSo we have decremented 1 from your input to create the chessboard\n\n')
};

console.log('\n');

for (let i = 1; i <= boardOrder; i++) {
    letsCreateChessBoard(i, boardOrder);
}

function letsCreateChessBoard(rowNumber, boardSize) {

    if (!isNaN(rowNumber) && !isNaN(boardSize) && rowNumber <= boardSize) {

        if (rowNumber % 2 == 1) {
            console.log(`\t${fontOne}\t${fontTwo}`.repeat(boardSize / 2));
        } else {
            console.log(`\t${fontTwo}\t${fontOne}`.repeat(boardSize / 2));
        }
    }
    console.log('\n')
}